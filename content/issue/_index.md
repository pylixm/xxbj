---
title: "Current issue"
date: 2021-03-05T23:45:29+08:00
comments: true
displayCopyright: true 
---

<div style="text-align: center">杂志仅供个人学习研究使用，请理性、客观、辩证阅读。</div>

<img width=300 height=400 src='https://gitee.com/pylixm/picture/raw/master/2021-3-5/1614953253970-image.png'/>


> **打印版：** https://pan.baidu.com/s/102w4SXLUiNjEVDfWCZjIcQ 提取码: 8gx8 
> 
> **PDF原版：** https://pan.baidu.com/s/1lNc1vPpjHBiqeZkpQ43KCw 提取码: tbnw
> 
> **音频：** https://pan.baidu.com/s/1QmagR_q3LYgrBRkZfcdfYw 提取码: ueme 

> 整理不易，如果我的工作帮助到你，恰好你的心情很好，你可以请我喝一杯 ☕，赞助下本站，将有「合集」赠送。<a href='/donate/'><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="icon donate"><path d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"></path></svg> Donate</a>